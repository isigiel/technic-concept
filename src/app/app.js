import angular from 'angular';
import ngMaterial from 'angular-material';
import ngAnimate from 'angular-animate';
import ngAria from 'angular-aria';
import ngMessages from 'angular-messages';
import ngSanitize from 'angular-sanitize';
import uirouter from 'angular-ui-router';
import 'restangular/dist/restangular';
import 'lodash';
import 'angular-material/modules/scss/angular-material.scss';

import states from './states';
import '../style/app.scss';
import '../style/animate.css';

class AppCtrl {
  constructor ($http) {
    'ngInject';
    var proxy = 'http://bochen415.info/loggify.php?url=';
    this.viewmode = false;
    this.selectedPack = {};
    console.log(proxy+'http://technicpack.net/search/modpacks/ta/?s=&p=1&c=0&se=0&st=popular&_=1464593195706');
    $http.get(proxy+'http://technicpack.net/search/modpacks/ta/?s=&p=1&c=0&se=0&st=popular').then((res) => {
      let packs = res.data.modpacks;
      this.sets = [];
      while (packs.length > 1){
        this.sets.push(packs.splice(0,2));
      }
      console.log(this.packs);
    })
  }

  showPack (pack) {
    this.viewmode = true;
    this.selectedPack = pack;
    pack.active = true;
  }

  showList () {
    this.viewmode = false;
    this.selectedPack.active = false;
  }
}

let app = {
  template: require('./app.ng.html'),
  controller: AppCtrl
};

angular
  .module('app', [
    ngAnimate,
    ngAria,
    ngMaterial,
    ngMessages,
    ngSanitize,
    uirouter,
    'restangular'
  ])
  .component('app', app)
  .config(($mdThemingProvider, $mdIconProvider, $locationProvider, RestangularProvider, $urlRouterProvider,  $stateProvider) => {
    'ngInject';
    RestangularProvider.setBaseUrl('http://localhost:3000');

    $locationProvider
      .html5Mode(false);
    $mdIconProvider
      .iconSet('icons', 'ico/icons.svg');
    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('blue-grey')
      .dark();

    //$urlRouterProvider.otherwise("/downloads");
    states($stateProvider);
  });