export default function ($stateProvider) {
  $stateProvider
    .state('downloads', {
      url: "/downloads",
      component: "downloads"
    });
}